With cli:
Clone project, navigate to project folder, type './gradlew assembleDebug'.
To install to install the app on a running device or emulator, yype './gradlew installDebug'.

With Android Studio:
Clone the project, open it with Android Studio and run.

Steps I used to create the app:
1. Listed the requirements
2. Decided on a pattern, in this case MVVM (model view viewmodel)
3. Created the models (Player, Cell, Game) => did not create a Board model because it would render the Game model almost useless. This would have been a good decision in bigger games.
=> Normally I would write tests at this moment. However because the app was so small, I started to create the layout
4. Created a basic looking layout: recyclverview with gridlayoutmanager, a small text and a refresh button
5. Created the glue, viewmodel.
6. Created the tests for all the models
7. Polished the layout.

This repo only contains one git commit. Normally, I would create a commit on a develop branch for every step I listed above (starting from the third step). For bigger applications, I would use feature branches.