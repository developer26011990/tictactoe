package be.test.tictactoe

import be.test.tictactoe.model.Player
import org.junit.Assert.*
import org.junit.Test

class PlayerUnitTest {

    @Test
    fun testEquality() {
        val playerA = Player("A")
        val playerB = Player("B")

        assertNotEquals(playerA, playerB)
        assertEquals(playerA, Player("A"))
    }

}