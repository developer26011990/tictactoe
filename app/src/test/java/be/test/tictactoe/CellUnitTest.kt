package be.test.tictactoe

import be.test.tictactoe.model.Cell
import be.test.tictactoe.model.Player
import org.junit.Test

import org.junit.Assert.*


class CellUnitTest {

    @Test
    fun testInitialization() {
        val cell = Cell()
        assertTrue(cell.isEmpty())
    }

    @Test
    fun testAddPlayer() {
        val cell = Cell()
        val player = Player("0")

        cell.setPlayer(player)

        assertFalse(cell.isEmpty())
        assertEquals(cell.getPlayer(), player)
    }

    @Test
    fun testAddMultiplePlayers() {
        val cell = Cell()
        val player1 = Player("0")
        val player2 = Player("1")

        cell.setPlayer(player1)
        cell.setPlayer(player2)

        assertFalse(cell.isEmpty())
        assertEquals(cell.getPlayer(), player1)
    }

    @Test
    fun testClearPlayer() {
        val cell = Cell()
        val player1 = Player("0")
        val player2 = Player("1")

        cell.setPlayer(player1)
        assertFalse(cell.isEmpty())
        assertEquals(cell.getPlayer(), player1)

        cell.clear()
        assertTrue(cell.isEmpty())

        cell.setPlayer(player2)
        assertFalse(cell.isEmpty())
        assertEquals(cell.getPlayer(), player2)
    }

}