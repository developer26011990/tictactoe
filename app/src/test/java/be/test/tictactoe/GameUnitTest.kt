package be.test.tictactoe

import be.test.tictactoe.model.Game
import be.test.tictactoe.model.Player
import org.junit.Assert.*
import org.junit.Test

class GameUnitTest {

    @Test
    fun testGameInitialization() {
        val game = Game()
        assertEquals(game.getCurrentPlayer(), Player("0"))
        for (cell in game.getBoardCells()) {
            assertTrue(cell.isEmpty())
        }
        assertNull(game.getWinner())
        assertFalse(game.isBoardFull())
    }

    @Test
    fun testValidSelection() {
        val game = Game()

        assertFalse(game.isValidSelection(-1, 0))
        assertFalse(game.isValidSelection(0, -1))
        assertFalse(game.isValidSelection(0, 3))
        assertFalse(game.isValidSelection(3, 2))
        assertFalse(game.isValidSelection(12, 4))

        for (row in 0..2) {
            for (col in 0..2) {
                assertTrue(game.isValidSelection(row, col))
            }
        }
    }

    @Test
    fun testGameReset() {
        val game = Game()

        game.select(0,0)
        game.select(1,0)
        game.select(2,0)
        game.reset()

        assertEquals(game.getCurrentPlayer(), Player("0"))
        for (cell in game.getBoardCells()) {
            assertTrue(cell.isEmpty())
        }
        assertNull(game.getWinner())
        assertFalse(game.isBoardFull())
    }

    @Test
    fun testSelection() {
        val game = Game()

        game.select(0,0)
        assertEquals(game.getCurrentPlayer(), Player("1"))
        assertFalse(game.getBoardCells()[0].isEmpty())
        assertNull(game.getWinner())
        assertFalse(game.isBoardFull())
        assertFalse(game.isValidSelection(0, 0))
    }

    @Test
    fun testDraw() {
        val game = Game()

        game.select(0,0)
        game.select(1,0)
        game.select(2,0)

        game.select(0,2)
        game.select(1,2)
        game.select(2,2)

        game.select(0,1)
        game.select(1,1)
        game.select(2,1)

        assertTrue(game.isBoardFull())
        assertNull(game.getWinner())
    }

    @Test
    fun testVerticalWin() {
        val game = Game()

        for (col in 0..2) {
            game.select(0, col)
            game.select(0, (col + 1)%3)

            game.select(1, col)
            game.select(1, (col + 1)%3)

            game.select(2, col)

            assertEquals(Player("0"), game.getWinner())
            game.reset()
        }
    }

    @Test
    fun testHorizontalWin() {
        val game = Game()

        for (row in 0..2) {
            game.select(row,0)
            game.select((row + 1)%3,0)

            game.select(row,1)
            game.select((row + 1)%3,1)

            game.select(row,2)

            assertEquals(Player("0"), game.getWinner())
            game.reset()
        }
    }

    @Test
    fun testDiagonalWin() {
        val game = Game()

        game.select(0,0)
        game.select(0,1)
        game.select(1,1)
        game.select(0, 2)
        game.select(2,2)

        assertEquals(Player("0"), game.getWinner())
        game.reset()

        game.select(0,2)
        game.select(0,0)
        game.select(1,1)
        game.select(0,1)
        game.select(2,0)

        assertEquals(Player("0"), game.getWinner())
    }
}