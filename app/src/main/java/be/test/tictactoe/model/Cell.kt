package be.test.tictactoe.model

class Cell {

    private var player: Player? = null

    fun getPlayer() = player

    fun setPlayer(player: Player) {
        if (isEmpty()) {
            this.player = player
        }
    }

    fun isEmpty() = player == null

    fun clear() {
        player = null
    }

}