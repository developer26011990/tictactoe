package be.test.tictactoe.model

import java.util.Arrays.asList
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class Game {

    private val player1 = Player("0")
    private val player2 = Player("1")

    private var currentPlayer = player1

    private val board = Array(3) { Array(3) { Cell() } }

    fun select(row: Int, col: Int) {
        if (isValidSelection(row, col)) {
            board[row][col].setPlayer(currentPlayer)
            currentPlayer = if (currentPlayer == player1) player2 else player1
        }
    }

    fun isValidSelection(row: Int, col: Int): Boolean {
        return getWinner() == null && row >= 0 && row < board.size && col >= 0 && col < board.size && board[row][col].isEmpty()
    }

    fun reset() {
        board.forEach { row -> row.forEach { it.clear() } }
        currentPlayer = player1
    }

    fun isBoardFull() = board.all { row -> row.all { !it.isEmpty() } }

    fun getCurrentPlayer() = currentPlayer

    fun getBoardCells(): List<Cell> {
        val cells = ArrayList<Cell>()
        for (cellArray in board) {
            cells.addAll(cellArray.asList())
        }
        return cells
    }

    fun getWinner(): Player? {
        //check horizontal winner
        for (i in 0..2) {
            val player = board[i][0].getPlayer()
            if (player != null && player == board[i][1].getPlayer() && player == board[i][2].getPlayer()) {
                return player
            }
        }

        //check vertical winner
        for (i in 0..2) {
            val player = board[0][i].getPlayer()
            if (player != null && player == board[1][i].getPlayer() && player == board[2][i].getPlayer()) {
                return player
            }
        }

        //check diagonal winner
        val player = board[1][1].getPlayer()
        if (player == board[0][0].getPlayer() && player == board[2][2].getPlayer()) {
            return player
        }
        if (player == board[0][2].getPlayer() && player == board[2][0].getPlayer()) {
            return player
        }

        return null
    }

}