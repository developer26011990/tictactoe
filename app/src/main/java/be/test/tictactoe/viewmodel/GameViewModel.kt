package be.test.tictactoe.viewmodel

import androidx.lifecycle.ViewModel
import be.test.tictactoe.model.Game
import androidx.lifecycle.MutableLiveData
import be.test.tictactoe.model.Cell
import be.test.tictactoe.model.Player

class GameViewModel : ViewModel() {

    val gameFinishedLiveData: MutableLiveData<Player?> = MutableLiveData()
    val playerTurnLiveData: MutableLiveData<Player> = MutableLiveData()
    val boardLiveDate: MutableLiveData<List<Cell>> = MutableLiveData()

    private val game = Game()

    fun select(row: Int, col: Int) {
        if (game.isValidSelection(row, col)) {
            game.select(row, col)
            refresh()
        }
    }

    fun reset() {
        game.reset()
        refresh()
    }

    fun refresh() {
        val player = game.getWinner()
        when {
            player != null -> gameFinishedLiveData.value = player
            game.isBoardFull() -> gameFinishedLiveData.value = null
            else -> playerTurnLiveData.value = game.getCurrentPlayer()
        }
        boardLiveDate.value = game.getBoardCells()
    }
}
