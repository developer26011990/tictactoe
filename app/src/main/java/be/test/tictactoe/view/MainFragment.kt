package be.test.tictactoe.view

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import be.test.tictactoe.R
import be.test.tictactoe.viewmodel.GameViewModel
import kotlinx.android.synthetic.main.main_fragment.*
import androidx.recyclerview.widget.GridLayoutManager

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: GameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(GameViewModel::class.java)

        cellList.layoutManager = GridLayoutManager(context, 3)

        viewModel.playerTurnLiveData.observe(this, Observer {
            when {
                it.id == "0" -> gameStatus.text = getString(R.string.game_turn_player1)
                else -> gameStatus.text = getString(R.string.game_turn_player2)
            }
            main.setBackgroundResource(R.color.background)
            gameStatus.setTextColor(ContextCompat.getColor(context!!, R.color.black))
        })

        viewModel.gameFinishedLiveData.observe(this, Observer {
            when {
                it == null -> gameStatus.text = getString(R.string.game_draw)
                it.id == "0" -> gameStatus.text = getString(R.string.game_won_player1)
                else -> gameStatus.text = getString(R.string.game_won_player2)
            }
            main.setBackgroundResource(R.color.background_finished)
            gameStatus.setTextColor(ContextCompat.getColor(context!!, R.color.white))
        })

        viewModel.boardLiveDate.observe(this, Observer {
            cellList.adapter = CellGridAdapter(it) { row, col -> viewModel.select(row, col) }
        })

        gameButton.setOnClickListener { viewModel.reset() }
    }

    override fun onResume() {
        super.onResume()
        viewModel.refresh()
    }

}