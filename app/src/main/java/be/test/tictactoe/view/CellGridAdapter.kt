package be.test.tictactoe.view

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.TextView
import androidx.core.content.ContextCompat
import be.test.tictactoe.R
import be.test.tictactoe.model.Cell

class CellGridAdapter(private val cells: List<Cell>, val listener: (Int, Int) -> Unit) : RecyclerView.Adapter<CellGridAdapter.CellViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CellViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val rootView = inflater.inflate(R.layout.cell_view, parent, false)
        return CellViewHolder(rootView)
    }

    override fun getItemCount() = cells.size

    override fun onBindViewHolder(holder: CellViewHolder, position: Int) {
        holder.bind(cells[position], position/3, position%3)
    }

    inner class CellViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var view: TextView = itemView.findViewById(R.id.cell)
        var row: Int = 0
        var col: Int = 0

        init {
            view.setOnClickListener { listener.invoke(row, col) }
        }

        fun bind(cell: Cell, row: Int, col: Int) {
            this.row = row
            this.col = col
            when {
                cell.isEmpty() -> {
                    view.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorEmpty))
                    view.text = ""
                }
                cell.getPlayer()?.id == "0" -> {
                    view.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorPlayer1))
                    view.text = "X"
                }
                else -> {
                    view.setBackgroundColor(
                        ContextCompat.getColor(
                            itemView.context,
                            R.color.colorPlayer2
                        )
                    )
                    view.text = "O"
                }
            }
        }
    }
}